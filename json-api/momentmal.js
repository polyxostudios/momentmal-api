/*
 * Momentmal Endless Gallery
 * Author: Robert Gühne
 *
 * This file contains the Endless Scrolling Gallery
 * To be fixed: Replace skrollr
 * don't use DOM for data storing move to Angular JS
*/

var posts = 0; //Post counter (replace with .length if no IE8 support needed)
var s; //Skrollr
var showimages = 2; //How many images are visible in viewport
var prevScrollView = 1;

//Create list in DOM from JSON
function loadMomentmal() {
    $.ajax({
            type: "GET",
            cache: false,
            dataType: 'json',
            url: 'images.json'
        })
        .done(function(data) {

            var source = $("#entry-template").html();
            var template = Handlebars.compile(source);
            var html;

            $.each(data, function(i, item) {
                item.number = i;
                html += template(item);
                posts++;
            });

            $("#momentmal").html(html);

            scrollLoader();
        });
}

// Load thumbnails based on scrolling position
function showImages(scrollPos, scrollMaxPos) {

    var scrollView = Math.round(scrollPos / scrollMaxPos * posts);

    if (prevScrollView != scrollView) {
        for (var i = 0; i < showimages; i++) {
            // Load image if class visible not set
            if (!jQuery("#moment-" + (scrollView + i)).hasClass('visible')) {
                var thumb = jQuery("#moment-" + (scrollView + i)).data("image");
                jQuery("#moment-" + (scrollView + i)).attr('src', thumb).addClass('visible');
            }
        }
        prevScrollView = scrollView;
    }
}

// Initialize Skrollr
function scrollLoader() {
    // Load first images
    showImages(0, 1000);
    s = skrollr.init({
        forceHeight: false,
        mobileCheck: function() {
            //hack - forces mobile version to be off
            return false;
        },
        render: function() {
            showImages(this.getScrollTop(), this.getMaxScrollTop());
        }
    });
}

loadMomentmal();
